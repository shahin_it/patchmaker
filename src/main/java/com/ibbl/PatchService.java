package com.ibbl;

import com.ibbl.utils.PatchBean;
import com.ibbl.utils.PatchUtil;
import com.ibbl.utils.PathUtils;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Collectors;

class PatchService {
    String slash = File.separator;
    PatchBean patchBean;

    public PatchService(PatchBean patchBean) {
        this.patchBean = patchBean;
    }


    public String makePatch() throws IOException {

        int i = 0, j = 0;
        StringBuilder logs = new StringBuilder();

        List<String> lines = Arrays.asList(patchBean.getSrc().split("\\n"));
        List<Map<String, String>> pathInfo = resolveSource(lines);

        for (Map<String, String> it : pathInfo) {
            File srcFile = new File(it.get("src"));
            File patchFile = new File(it.get("target"));

            if (srcFile.exists()) {

                String ext = PathUtils.getFileExtension(srcFile);

                if (!patchFile.getParentFile().exists()) {
                    patchFile.getParentFile().mkdirs();
                }

                try {
                    switch (ext) {
                        case "class":
                            FileUtils.copyFile(srcFile, patchFile, true);
                            break;
                        default:
                            FileUtils.copyFile(srcFile, patchFile, true);
                            break;
                    }

                    i++;
                } catch (IOException e) {
                    e.printStackTrace();
                }
                logs.append("Copied[" + i + "]!: " + patchFile.getPath() + "\n");

            }
            else {
                j++;
                logs.append("Source file not found: " + srcFile.getPath() + "\n");
            }
        }

        logs.append("Succeeded: " + i + "\n");
        logs.append("Failed: " + j + "\n");
        return logs.toString();
    }

    private List<Map<String, String>> resolveSource(List<String> srcLine) {
        List<Map<String, String>> patchInfo = new ArrayList<>();
        String projectDir = FilenameUtils.normalize(patchBean.getProjectDir() + slash);
        String projectName = PatchUtil.getDirName(projectDir);
        String classpath = FilenameUtils.normalize(patchBean.getClassPath());
        String artifactDir = FilenameUtils.normalize(patchBean.getArtifactDir() + slash);
        String patchDir = artifactDir + "patch" + slash;
        SimpleDateFormat formatter = new SimpleDateFormat("ddMMyyyy_hhmmss");
        Calendar cal = Calendar.getInstance();
        String targetDir = patchDir + patchBean.getPatchName() + "_patch_" + formatter.format(cal.getTime()) + slash;

        srcLine.forEach(line -> {
            line = FilenameUtils.normalize(line.replaceAll("\\s", ""));
            if (line.equals("") || line.startsWith("#")) {
                return;
            }
            else if (FilenameUtils.wildcardMatchOnSystem(line, ("*" + slash + projectName + slash + "*"))) {
                String[] rs = line.split(projectName + "\\" + slash);
                if (rs.length >= 2) {
                    line = rs[1];
                }
//                    line = rs.length < 2 ?:rs[1];
            }

            if (line.startsWith("src" + slash)) {
                line = line.replace("src" + slash, classpath);
                if (PathUtils.hasExtension(line, "java")) {
                    line = FilenameUtils.removeExtension(line) + ".class";
                }
            }

            String srcAbsPath = artifactDir + line;
            File srcFile = new File(srcAbsPath);

            if (srcFile.exists()) {
                if (srcFile.isDirectory()) {
                    List<String> childPaths = PathUtils.getFileList(srcFile).stream()
                            .map(it -> it.getPath().replace(new File(artifactDir).getPath(), ""))
                            .collect(Collectors.toList());

                    patchInfo.addAll(resolveSource(childPaths));
                }
                else {
                    String targetPath = FilenameUtils.normalize(targetDir + line);
                    patchInfo.add(Map.of("src", srcFile.getAbsolutePath(), "target", targetPath));
                }
            }

        });
        return patchInfo;
    }

}
