package com.ibbl.utils;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang3.StringUtils;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.util.List;
import java.util.Properties;

public class PatchUtil {

    public static Properties getProp(String fileName) {
        Properties prop = new Properties();
        try {
            InputStream stream = getStreamFromResources(fileName);
            prop.load(stream);
            stream.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return prop;
    }

    private static ClassLoader getClassLoader() {
        return PatchUtil.class.getClassLoader();
    }

    private static String resolvePath(String fileName) {
        return getClassLoader().getResource(fileName).getFile();
    }

    private static InputStream getStreamFromResources(String fileName) {
        InputStream resource = getClassLoader().getResourceAsStream(fileName);
        try {
            if (resource.available() <= 0) {
                throw new IOException(fileName + ": file is not found!");
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        return resource;
    }

    public static String getDirName(String dir) {
        String dirName = FilenameUtils.getBaseName(dir);
        if(StringUtils.isBlank(dirName)) {
            dir = dir.substring(0, dir.length()-1);
            dirName = FilenameUtils.getBaseName(dir);
        }
        return dirName;
    }

    public static List<String> readLine(String fileName) {
        try {
            return FileUtils.readLines(new File(resolvePath(fileName)), StandardCharsets.UTF_8);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static String readFile(String fileName) {
        try {
            return FileUtils.readFileToString(new File(resolvePath(fileName)), StandardCharsets.UTF_8);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static void UpdateProps(String fileName, Properties props) {
        try {
            OutputStream out = new FileOutputStream(resolvePath(fileName));
            props.store(out, null);
            out.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
