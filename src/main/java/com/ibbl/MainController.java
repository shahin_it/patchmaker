package com.ibbl;

import com.ibbl.utils.PatchBean;
import com.ibbl.utils.PatchUtil;
import javafx.event.Event;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.stage.DirectoryChooser;
import org.apache.commons.lang3.StringUtils;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.Properties;
import java.util.ResourceBundle;

public class MainController implements Initializable {
    @FXML
    public TextField projectDirTxt;
    @FXML
    public TextField patchNameTxt;
    @FXML
    public TextField artifactDirTxt;
    @FXML
    public TextField classPathTxt;
    @FXML
    public Label errorLabel;
    @FXML
    public Label successLabel;
    @FXML
    public TextArea logArea;
    @FXML
    public TextArea srcPathTxt;
    Properties prop;

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        String javaVersion = System.getProperty("java.version");
        String javafxVersion = System.getProperty("javafx.version");
        prop = PatchUtil.getProp("config.properties");
        artifactDirTxt.textProperty().addListener((observable, oldValue, newValue) -> {
            if (StringUtils.isNotBlank(newValue)) {
                String dirName = PatchUtil.getDirName(newValue);
                patchNameTxt.setText(dirName);
            }
        });

        String projectDir = prop.getProperty("project.dir");
        String artifactDir = prop.getProperty("artifact.dir");
        String patchName = prop.getProperty("patch.name");
        String classPath = prop.getProperty("class.path");
        String src = PatchUtil.readFile("source.txt");
        if (StringUtils.isNotBlank(projectDir)) {
            projectDirTxt.setText(projectDir);
            artifactDirTxt.setText(artifactDir);
            patchNameTxt.setText(patchName);
            classPathTxt.setText(classPath);
            srcPathTxt.setText(src);
        }
    }

    @FXML
    public void browsePath(Event e) {
        Button button = (Button) e.getSource();
        DirectoryChooser directoryChooser = new DirectoryChooser();
        File selectedDirectory = directoryChooser.showDialog(this.projectDirTxt.getContextMenu());

        if (selectedDirectory != null) {
            if ("projectDirBtn".equalsIgnoreCase(button.getId())) {
                projectDirTxt.setText(selectedDirectory.getAbsolutePath());
            }
            else if ("artifactDirBtn".equalsIgnoreCase(button.getId())) {
                artifactDirTxt.setText(selectedDirectory.getAbsolutePath());
            }
        }
    }

    @FXML
    public void build(Event e) {
        String projectDir = projectDirTxt.getText();
        String patchName = patchNameTxt.getText();
        String artifactDir = artifactDirTxt.getText();
        String classPath = classPathTxt.getText();
        String src = srcPathTxt.getText();

        if (StringUtils.isBlank(projectDir)) {
            errorLabel.setText("Project directory is required");
        }
        else if (StringUtils.isBlank(patchName)) {
            errorLabel.setText("Patch name is required");
        }
        else if (StringUtils.isBlank(artifactDir)) {
            errorLabel.setText("Artifact name is required");
        }
        else if (StringUtils.isBlank(classPath)) {
            errorLabel.setText("Class/build path is required");
        }
        else if (StringUtils.isBlank(src)) {
            errorLabel.setText("Source absolute path is required");
        }
        else {
            errorLabel.setText("");
            try {
                PatchBean patchBean = new PatchBean();
                patchBean.setProjectDir(projectDir);
                patchBean.setArtifactDir(artifactDir);
                patchBean.setPatchName(patchName);
                patchBean.setClassPath(classPath);
                patchBean.setSrc(src);
                PatchService patchService = new PatchService(patchBean);
                String log = patchService.makePatch();
                logArea.setText(log);

                prop.setProperty("project.dir", projectDir);
                prop.setProperty("artifact.dir", artifactDir);
                prop.setProperty("patch.name", patchName);
                prop.setProperty("class.path", classPath);
                PatchUtil.UpdateProps("config.properties", prop);
            } catch (IOException ioException) {
                ioException.printStackTrace();
            }
        }
    }


}